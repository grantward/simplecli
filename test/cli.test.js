//Evaluate broad functionality of cli module
'use-strict';

const cli = require('../src/cli.js');


const testData = {
  good: {
    options: [
      [
        {
          'dest': 'a',
          'short': 'a',
          'long': 'longa',
          'nargs': 0,
        },
      ],
      [{dest: 'bar', long: 'bar', nargs: 0}],
      [
        {
          'dest': 'foo',
          'short': 'f',
          'nargs': '+',
          'required': true,
          'desc': 'Provides foo capabilities',
        },
        {
          'dest': 'bar',
          'short': 'b',
          'long': 'bar',
          'nargs': 2,
          'required': false,
          'desc': 'Provides bar capabilities if specified',
        },
      ],
      [
        {
          'dest': 'transformed',
          'short': 't',
          'nargs': 1,
          'required': true,
          'transform': parseInt,
        },
      ],
      [
        {
          'dest': 'opt1',
          'default': 0,
        },
        {
          'dest': 'opt2',
          'default': true,
        },
      ]
    ],
    positionals: [
      'rest',
      [],
      [
        {
          'dest': 'inputFile',
          'required': true,
          'desc': 'The file to read',
        },
        {
          'dest': 'outputFile',
        },
      ],
      [],
      [],
    ],
    args: [
      ['-a', 'pos1', 'pos2'],
      ['--bar'],
      ['file.txt', '-f', 'baz', 'baz2', '--bar', 'a', '2'],
      ['-t', '-55'],
      [],
    ],
  },
  bad: {
    options: [
      //Missing dest
      [
        {},
      ],
      //Missing foo
      [
        {
          'dest': 'foo',
          'required': true,
        },
      ],
      //Conflicting dest
      [
        {
          'dest': 'foo',
        },
        {
          'dest': 'foo',
        },
      ],
      //Too many positionals
      [],
    ],
    positionals: [
      [],
      [],
      [],
      [],
    ],
    args: [
      [],
      [],
      [],
      ['foo'],
    ],
  }
};

const parse = (goodBad, idx) => cli.parseArgs(
  testData[goodBad].args[idx],
  testData[goodBad].options[idx],
  testData[goodBad].positionals[idx]);

const usage = (goodBad, idx) => cli.usageStr(
  'program.js',
  testData[goodBad].options[idx],
  testData[goodBad].positionals[idx]);


describe('Test parse arguments', () => {
  test('Parse good args', () => {
    const parsed = parse('good', 0);
    expect(parsed.a).toBe(true);
    expect(parsed.rest.length).toBe(2);
    expect(parsed.rest[1]).toEqual('pos2');
  });
  test('Parse long flag', () => {
    const parsed = parse('good', 1);
    expect(parsed.bar).toBe(true);
  });
  test('Parse good args 2', () => {
    const parsed = parse('good', 2);
    expect(parsed.foo.length).toBe(2);
  });
  test('Parse transform single', () => {
    const parsed = parse('good', 3);
    expect(parsed.transformed).toBe(-55);
  });
  test('Parse with defaults', () => {
    const parsed = parse('good', 4);
    expect(parsed.opt1).toBe(0);
    expect(parsed.opt2).toBe(true);
  });
  test('Parse bad args', () => {
    expect(() => parse('bad', 0)).toThrow(/dest.*required/);
  });
  test('Parse missing required', () => {
    expect(() => parse('bad', 1)).toThrow(/foo.*required/);
  });
  test('Parse duplicate dest', () => {
    expect(() => parse('bad', 2)).toThrow(/duplicate/i);
  });
  test('Parse too many positional', () => {
    expect(() => parse('bad', 3)).toThrow(/positional/i);
  });
});

describe('Test generation of usage strings', () => {
  test('Good usage string', () => {
    const usageStr = usage('good', 0);
    console.log(usageStr);
  });
  test('Good usage string 1', () => {
    const usageStr = usage('good', 1);
    console.log(usageStr);
  });
});
