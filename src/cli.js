//Provides CLI arg parsing
'use-strict';

const utils = require('@grantward/utils');


function parseArgs(args, options, positionals=[]) {
  validateOptions(options);
  const re = generateRegex(options);
  let collect = '';
  let positionalIdx = 0;
  let nargs = 0;
  let res = {};
  if (typeof positionals === 'string') {
    res[positionals] = [];
  }
  for (const argIdx in args) {
    const arg = args[argIdx];
    const matched = getTypeOfArg(re, arg);
    const type = matched.type;
    const match = matched.match;
    if (type === 'positional') {
      if (nargs) {
        //Collecting for an option
        if (!res[collect]) {
          res[collect] = [];
        }
        res[collect].push(arg);
        if (typeof nargs === 'number') {
          nargs -= 1;
        }
      } else {
        //Collecting positionals
        if (typeof positionals === 'string') {
          res[positionals].push(arg);
        } else {
          if (positionalIdx >= positionals.length) {
            throw `Too many positional arguments: ${positionals.length} allowed`;
          }
          res[positionals[positionalIdx].dest] = arg;
          positionalIdx += 1;
        }
      }
    } else {
      //Not a positional, cancel nargs
      if (nargs) {
        if (nargs === '+') {
          if (res[collect].length < 1) {
            throw `Argument(s) for ${collect} missing but required`;
          }
        }
        if (typeof nargs === 'number') {
          throw `${nargs} additional arguments for ${collect} required`;
        }
      }
      nargs = 0;
      if (type === 'shortFlags' || type === 'longFlags') {
        let flags = null;
        let propertyName = null;
        if (type === 'shortFlags') {
          flags = match.groups.flags;
          propertyName = 'short';
        } else {
          flags = [match.groups.flag];
          propertyName = 'long';
        }
        for (const flagIdx in flags) {
          const flag = flags[flagIdx];
          const option = findOption(options, propertyName, flag);
          res[option.dest] = true;
        }
      } else if (type === 'shortParams' || type === 'longParams') {
        const propertyName = (type === 'shortParams' ? 'short' : 'long');
        const option = findOption(options, propertyName, match.groups.param);
        collect = option.dest;
        nargs = option.nargs;
      } else {
        throw `Argument not understood: ${arg}`;
      }
    }
  }
  //Validation
  res = validateArguments(options, res);
  if (!(typeof positionals === 'string')) {
    for (const idx in positionals) {
      const positional = positionals[idx];
      if (positional.required) {
        if (!res.hasOwnProperty(positional.dest)) {
          throw `${positionals.length} positional arguments required`;
        }
      }
      if (positional.transform) {
        res[positional.dest] = positional.transform(res[positional.dest]);
      }
    }
  }
  return res;
}

function usageStr(prog, options, positionals=[]) {
  validateOptions(options);
  const lineSep = '\n\t';
  let lines = [`Usage: ${prog}`];
  function wrap(required, str) {
    const syms = {
      opt: {
        begin: '[',
        end: ']',
      },
      req: {
        begin: '<',
        end: '>',
      },
    };
    required = required ? 'req' : 'opt';
    return `${syms[required].begin}${str}${syms[required].end}`;
  }
  for (const idx in options) {
    const option = options[idx];
    lines.push(
      wrap(option.required, `-${option.short},--${option.long} ` +
        (option.nargs ?
          (typeof option.nargs === 'number' ?
            Array.from(
              {length: option.nargs},
              (_, idx) => wrap(true, `${option.dest}${idx+1}`))
            .join(',') :
            wrap((option.narg === '+'), `${option.dest} ...`)
          ) :
          ''
        )
      ) +
      (option.desc ? ` - ${option.desc}` : '')
    );
  }
  if (typeof positionals === 'string') {
    lines.push(wrap(false, wrap(false, positionals) + ' ...'));
  } else {
    for (const idx in positionals) {
      const positional = positionals[idx];
      lines.push(
        wrap(positional.required, positional.dest) +
        (positional.desc ? ` - ${positional.desc}` : '')
      );
    }
  }
  return lines.join(lineSep);
}

//Retrieve an option by a property
function findOption(options, propertyName, value) {
  for (const idx in options) {
    const option = options[idx];
    if (option[propertyName] === value) {
      return option;
    }
  }
  throw `No option corresponds to ${propertyName}=${value}`;
}

//return the type of the current arg
function getTypeOfArg(re, arg) {
  for (const type in re) {
    let match = re[type].exec(arg);
    if (match) {
      return {type: type, match: match};
    }
  }
  throw `Does not match allowed argument types: ${arg}`;
}

//throw exception if duplicate properties
function noDuplicateProps(objs, prop) {
  const objList = Array.from(objs, (obj) => obj[prop])
    .filter(prop => (typeof prop !== 'undefined'));
  const objSet = new Set(objList);
  if (objList.length !== objSet.size) {
    throw `CliError: Duplicates found in array: ${objList}`;
  }
}

//throw exception if options are not valid
function validateOptions(options) {
  noDuplicateProps(options, 'dest');
  noDuplicateProps(options, 'short');
  noDuplicateProps(options, 'long');
  for (const idx in options) {
    const option = options[idx];
    if (!option.dest) {
      throw "CliError: 'dest' property is required for each option";
    }
    if (option.short) {
      if (option.short.length > 1) {
        throw `CliError: Short option too long: ${option.short}`;
      }
    }
    //Validate / set default of nargs
    switch(typeof option.nargs) {
      case 'string':
        if (!utils.inArray(['*', '+'], option.nargs)) {
          throw `CliError: nargs: ${option.nargs} was not understood`;
        }
        break;
      case 'number':
        if (!utils.isWhole(option.nargs)) {
          throw `CliError: nargs: ${option.nargs} must be positive integer`;
        }
        break;
      case 'undefined':
        options[idx].nargs = 0;
        break;
      default:
        throw "CliError: nargs must be one of: positive integer, '*', or '+'";
    }
    //Validate transform functions
    if (option.transform) {
      if ((typeof option.transform) !== 'function') {
        throw 'CliError: transform must be function';
      }
    }
  }
}

//Throw exception if received arguments are not valid
function validateArguments(options, res) {
  for (const idx in options) {
    const option = options[idx];
    const nargs = option.nargs;
    if (res.hasOwnProperty(option.dest)) {
      //Ensure nargs satisfied
      if (nargs) {
        if (typeof nargs === 'string') {
          switch(nargs) {
            case '+':
              if (res[option.dest].length < 1) {
                throw `Argument: ${option.dest} requires at least 1 argument`;
              }
            case '*':
              break;
          }
        } else {
          if (res[option.dest].length !== nargs) {
            throw `Argument: ${option.dest} requires exactly ${nargs} arguments`;
          }
        }
      }
      //Do Transformations
      if (option.transform) {
        res[option.dest] = res[option.dest].map(
          each => option.transform(each));
      }
      //Consolidate single args
      if (option.nargs === 1) {
        res[option.dest] = res[option.dest][0];
      }
    } else {
      if (option.required) {
        throw `Argument: ${option.dest} is required`;
      }
      res[option.dest] = option.default;
    }
  }
  return res;
}

//Generate the regexes for options
function generateRegex(options) {
  let res = {};
  //flags take no arguments
  const flags = options.filter(opt => !(opt.nargs));
  res.shortFlags = new RegExp(
    '^-(?<flags>[' +
    Array.from(flags, flag => flag.short).join('') + ']+)$'
  );
  res.longFlags = new RegExp(
    '^--(?<flag>' +
    Array.from(flags, flag => flag.long).join('|') + ')$'
  );
  //these options require arguments
  const params = options.filter(opt => opt.nargs);
  res.shortParams = new RegExp(
    '^-(?<param>[' +
    Array.from(params, param => param.short).join('') + '])$'
  );
  res.longParams = new RegExp(
    '^--(?<param>' +
    Array.from(params, param => param.long).join('|') + ')$'
  );
  res.positional = /^.*$/;
  return res;
}


module.exports = {
  parseArgs,
  usageStr,
};
